clear
docker rm -f $(docker ps -aq)

if [ -f signature_backend/.env ]
then
  export $(cat signature_backend/.env | xargs)
fi

if [ -f signature_backend/.env.secrets ]
then
  export $(cat signature_backend/.env.secrets | xargs)
fi

cp ./signature_backend/infra/rabbitmq/definitions.json ./signature_backend/infra/rabbitmq/definitions.copy

sed -i 's/RABBITMQ_USER/'$RABBITMQ_USER'/g' ./signature_backend/infra/rabbitmq/definitions.copy
sed -i 's/RABBITMQ_PASSWORD/'$RABBITMQ_PASSWORD'/g' ./signature_backend/infra/rabbitmq/definitions.copy
sed -i 's/RABBITMQ_READ_EMAIL_QUEUE/'$RABBITMQ_READ_EMAIL_QUEUE'/g' ./signature_backend/infra/rabbitmq/definitions.copy
sed -i 's/RABBITMQ_READ_EMAIL_EXCHANGE/'$RABBITMQ_READ_EMAIL_EXCHANGE'/g' ./signature_backend/infra/rabbitmq/definitions.copy

docker-compose up --build

rm ./signature_backend/infra/rabbitmq/definitions.copy
