"""Specification document signature management entities
"""

from pydantic import BaseModel


class Senders(BaseModel):
    name: str
    email: str
    position: str
    company: str

    class Config:
        orm_mode = True


class Recipients(BaseModel):
    name: str
    email: str

    class Config:
        orm_mode = True


class TypeOfProcessPdfFiles(BaseModel):
    file_description: str
    id_tags_in_text: list[str]
    required_file: bool

    class Config:
        orm_mode = True


class TypesOfProcess(BaseModel):
    type_of_process: str
    department: str
    type_of_process_pdf_files: list[TypeOfProcessPdfFiles]

    class Config:
        orm_mode = True
