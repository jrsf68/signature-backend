# from abc import ABC, abstractmethod

# # Define an abstract base class
# class TypeOfProcess(ABC):

#     @abstractmethod
#     def save(type_of_process: dict):
#         pass

#     @abstractmethod
#     def get_by_id(id) -> dict:
#         pass

# # # Implement the interface
# # class TypeOfProcess(TypeOfProcess):

# #     def method1(self):
# #         print("Implementing method1")

# #     def method2(self, arg):
# #         print(f"Implementing method2 with argument: {arg}")

# # # Create an instance and use the interface methods
# # my_obj = MyClass()
# # my_obj.method1()
# # my_obj.method2("Hello, interface!")
