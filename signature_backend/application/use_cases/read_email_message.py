import pytest


@staticmethod
@pytest.mark.asyncio
async def test_read_message_email():

    message = await read_email_message()

    assert message


# from signature_backend.infra.consumer_message_mail.listen_queue import read_email

# import pytest


# # @staticmethod
# @pytest.mark.asyncio
# async def test_read_email_message():

#     message = await read_email_message()

#     assert message


async def read_email_message():
    return {'test:': 'ok'}


# async def read_email_message() -> set:
#     """Read message from brocker queue.

#     Returns:
#         set: serialized message

#     # >>> read_email_message('BrockerTest')
#     # {'test'}
#     """
#     message = None
#     print('\n\nWait to Server starting ...')
#     message = await read_email()
#     print(f'message read: {message}')
#     return message
