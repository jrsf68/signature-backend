"""Implenents types of process functions.
"""

from signature_backend.domain.entities import TypesOfProcess
from signature_backend.infra.repository.database import (
    create_in_database_type_of_process,
    get_from_database_type_of_process,
    read_from_database_type_of_process,
)


async def create_type_of_process(type_of_process: TypesOfProcess):
    print(f'\n\n\n\n\n\ntype_of_process: {type_of_process}')
    await create_in_database_type_of_process(type_of_process)


async def get_type_of_process(id: int) -> dict:
    return await get_from_database_type_of_process(id)


async def read_type_of_process(type_of_process: str) -> dict:
    return await read_from_database_type_of_process(type_of_process)
