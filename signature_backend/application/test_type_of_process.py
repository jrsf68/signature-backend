from random import randint

import pytest

from signature_backend.application.type_of_process import (
    create_type_of_process,
    read_type_of_process,
)


@pytest.mark.asyncio
async def test_create_type_of_process():
    test_number = randint(10, 1000)
    type_of_process = {
        'type_of_process': f'TEST{test_number}_RESCISÃO SEM JUSTA CAUSA',
        'department': 'Departamento de Pessoal',
        'type_of_process_pdf_files': [
            {
                'file_description': 'Termo de Rescisão de Contrato de Trabalho',
                'id_tags_in_text': ['TRCT'],
                'required_file': True,
            }
        ],
    }

    await create_type_of_process(type_of_process)
    result_set = await read_type_of_process(type_of_process['type_of_process'])

    assert (
        result_set['type_of_process']
        == f'TEST{test_number}_RESCISÃO SEM JUSTA CAUSA'
    )


@pytest.mark.asyncio
async def test_read_type_of_process():
    type_of_process = 'TEST_RESCISÃO SEM JUSTA CAUSA'

    result_set = await read_type_of_process(type_of_process)

    assert result_set == {
        'id': 1,
        'type_of_process': 'TEST_RESCISÃO SEM JUSTA CAUSA',
        'department': 'Departamento de Pessoal',
    }
