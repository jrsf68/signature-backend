"""Implement read message from email server
"""

from asyncio import run
from os import getenv
from pickle import dumps
from socket import setdefaulttimeout

from imap_tools import MailBox
from pika import BlockingConnection, ConnectionParameters, PlainCredentials
from rocketry import Rocketry

app = Rocketry()

setdefaulttimeout(1)  # email server request timeout


async def connect():
    print(f'Starting connect function ...')
    try:
        connection_parameters = ConnectionParameters(
            host=getenv('RABBITMQ_SERVER_URL'),
            port=getenv('RABBITMQ_SERVER_PORT'),
            credentials=PlainCredentials(
                username=getenv('RABBITMQ_USER'),
                password=getenv('RABBITMQ_PASSWORD'),
            ),
        )
        connection = BlockingConnection(connection_parameters)
        return connection
    except:
        raise Exception(f'Error connect RabbitMQ!')


async def publish_message(message):
    print(f'Starting publish_message function ...')
    try:
        connection = await connect()
        channel = connection.channel()
        channel.confirm_delivery()
    except Exception as e:
        print(e)
        raise Exception(f'Error create channel.')

    try:
        channel.exchange_declare(
            exchange=getenv('RABBITMQ_READ_EMAIL_EXCHANGE'),
            exchange_type='direct',
            durable=True,
        )
    except Exception as e:
        print(e)
        raise Exception(f'Error queue declare.')

    try:
        channel.basic_publish(
            exchange=getenv('RABBITMQ_READ_EMAIL_EXCHANGE'),
            body=message,
            routing_key=getenv('RABBITMQ_READ_EMAIL_QUEUE'),
        )
    except Exception as e:
        print(e)
        raise Exception(f'Error publish message.')

    connection.close()


async def connect_to_email_server(email_server):
    """Connet to email server.

    Args:
        email_server (str): email server' url.

    Returns:
        class 'imap_tools.mailbox.MailBox': connection object.
    # >>> conn = await connect_to_email_server('imap.gmail.com')
    # >>> type(conn)
    <class 'imap_tools.mailbox.MailBox'>
    """

    handler_email_connection = MailBox(email_server)
    return handler_email_connection


async def account_authentication(handler_connect, email, password):
    f"""authenticates to document signature management email

    Args:
        handler_connect (class 'imap_tools.mailbox.MailBox'): connection object.
        email (str): document signature management email
        password (str): document signature management email password

    Returns:
        class 'imap_tools.mailbox.MailBox': connection object.

    >>> conn = connect_to_email_server('imap.gmail.com')
    >>> conn = account_authentication(conn, {getenv('EMAIL_ACCOUNT')}, {getenv('EMAIL_PASSWORD')})
    >>> type(conn)
    <class 'imap_tools.mailbox.MailBox'>
    """

    print(f'Starting account_authentication function ...')
    handler_connect.login(email, password)
    return handler_connect


@app.task('every 5 minutes')
async def read_message():
    """Read and delete one message from email server.

    Returns:
        set: message.
    # >>> message = read_message()
    # >>> type(message)
    <class 'imap_tools.message.MailMessage'>
    """

    print(f'Starting read_message function ...')
    email_server = getenv('IMAP_EMAIL_SERVER')
    email = getenv('EMAIL_ACCOUNT')
    password = getenv('EMAIL_PASSWORD')
    try:
        conn = await connect_to_email_server(email_server)
        handler_connect = await account_authentication(conn, email, password)
        handler_message = None
        for handler_message in handler_connect.fetch():
            handler_connect.move(handler_connect.uids(1), 'Arquivado')
            # serialized_message = dumps(handler_message)
            serialized_message = "{'ricardo': 'ferreira'}"
            await publish_message(serialized_message.encode('utf-8'))
            handler_connect.logout()
            return handler_message
    except Exception as x:
        print(x)
        return False
    return None


if __name__ == '__main__':
    print(f'Starting read message from email server ...')
    app.run()
    print(f'Message read!')
