#!/bin/bash

while ! nc -z rabbitmq 5672; do sleep 30; done
python3 /signature_backend/infra/consumer_message_mail/listen_queue.py
