from psycopg2.extras import RealDictCursor

from signature_backend.domain.entities import TypesOfProcess
from signature_backend.infra.repository.database_adapters import (
    connection_to_database,
    insert_into_database,
)


async def create_in_database_type_of_process(type_of_process: dict):
    # type_of_process = type_of_process.dict()
    try:
        connection = await connection_to_database()
    except Exception as e:
        raise Exception(f'Error in connetion ({e}).')

    try:
        await insert_into_database(connection, type_of_process)
        for file in type_of_process['type_of_process_pdf_files']:
            await create_in_database_type_of_process_pdf_files(
                type_of_process['type_of_process'], file
            )

    except Exception as e:
        raise (f'Error to insert: ({e})')
        # raise Exception(e)

    connection.close()


async def create_in_database_type_of_process_pdf_files(
    type_of_process,
    type_of_process_pdf_files,
):
    connection = await connection_to_database()

    try:
        cursor = connection.cursor()
    except Exception as e:
        raise (f'Error in curso instance: ({e})')
        # raise Exception(e)

    tag = str(type_of_process_pdf_files['id_tags_in_text']).replace("'", '"')
    query = f"""INSERT INTO signature_backend.type_of_process_pdf_files
                (
                    type_of_process_id,
                    file_description,
                    id_tags_in_text,
                    required_file
                ) 
                VALUES (
                    (SELECT id 
                    FROM signature_backend.types_of_process
                        WHERE type_of_process = 
                        '{type_of_process}'),
                    '{type_of_process_pdf_files['file_description']}',
                    '{tag}',
                    '{type_of_process_pdf_files['required_file']}'
                );
            """
    try:
        cursor.execute(query)
    except Exception as e:
        raise (f'Error cursor insert type_of_process_pdf_files: ({e})')

    connection.commit()

    connection.close()


async def get_from_database_type_of_process(id: int) -> dict:
    connection = await connection_to_database()

    try:
        cursor = connection.cursor(cursor_factory=RealDictCursor)
    except Exception as e:
        raise (f'Error in curso instance: ({e})')
        # raise Exception(e)

    query = f"""SELECT * FROM signature_backend.types_of_process
                        WHERE id = '{id}';
                    """
    try:
        cursor.execute(query)
        result_set = cursor.fetchall()
        result_set = [dict(row) for row in result_set][0]
    except Exception as e:
        raise (f'Error cursor execution: ({e})')

    connection.close()

    return result_set


async def read_from_database_type_of_process(type_of_process: str) -> dict:
    connection = await connection_to_database()

    try:
        cursor = connection.cursor(cursor_factory=RealDictCursor)
    except Exception as e:
        raise (f'Error in curso instance: ({e})')
        # raise Exception(e)

    query = f"""SELECT * FROM signature_backend.types_of_process
                        WHERE type_of_process = '{type_of_process}';
                    """
    try:
        cursor.execute(query)
        result_set = cursor.fetchall()
        result_set = [dict(row) for row in result_set][0]
    except Exception as e:
        raise (f'Error cursor execution: ({e})')

    connection.close()

    return result_set
