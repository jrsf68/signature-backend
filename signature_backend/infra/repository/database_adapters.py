"""Implements database adapters functions
"""

import psycopg2

from signature_backend.domain.entities import TypesOfProcess


async def connection_to_database():
    try:
        connection = psycopg2.connect(
            host='localhost',
            database='postgres',
            user='postgres',
            password='123456',
            port='5432',
        )
    except Exception as e:
        print(f'Error in connection init: ({e})')
        raise Exception(e)

    return connection


async def insert_into_database(
    connection: any, type_of_process: TypesOfProcess
):
    try:
        cursor = connection.cursor()
    except Exception as e:
        raise Exception(f'Error in curso instance: ({e})')
    print(f'\n\n\n\n\n\ntype_of_process: {type_of_process}')
    insert_query = f"""INSERT INTO signature_backend.types_of_process
                        (
                            type_of_process, 
                            department
                        ) 
                        VALUES (
                            '{type_of_process['type_of_process']}',
                            '{type_of_process['department']}'
                        );
                    """
    print(f'\n\ninsert_query: {insert_query}')
    try:
        cursor.execute(insert_query)
    except Exception as e:
        raise Exception(f'Error cursor execution: ({e})')

    connection.commit()
