from random import randint

import httpx
import pytest
from fastapi.testclient import TestClient

from signature_backend.domain.entities import (
    TypeOfProcessPdfFiles,
    TypesOfProcess,
)
from signature_backend.main import app

client = TestClient(app)


# @pytest.fixture(scope='module')
# def server_url():
#     return 'http://localhost:8000'


# def test_server_is_running(server_url):
#     with httpx.Client() as client:
#         response = client.get(server_url)
#     assert response.status_code == 200


# @pytest.mark.asyncio
# async def test_read_root():
#     endpoint = '/'

#     response = client.get(endpoint)

#     assert response.status_code == 200
#     assert response.json() == {'status': 'Server it is running!'}


# @pytest.mark.asyncio
# async def test_create_type_of_process():
#     test_number = randint(10, 1000)
#     url = '/type_of_process/'
#     pdf_file = TypeOfProcessPdfFiles(
#         file_description='Termo de Rescisão de Contrato de Trabalho',
#         id_tags_in_text=['TRCT'],
#         required_file=True,
#     )
#     payload = TypesOfProcess(
#         type_of_process=f'TEST{test_number}_RESCISÃO SEM JUSTA CAUSA',
#         department='Departamento de Pessoal',
#         type_of_process_pdf_files=[pdf_file],
#     )

#     response = client.post(url=url, json=payload.dict())

#     assert response.status_code == 200


# @pytest.mark.asyncio
# async def test_read_type_of_process():
#     id = 1
#     endpoint = f'/type_of_process/{id}'

#     response = client.get(endpoint)

#     assert response.status_code == 200
#     assert response.json() == {
#         'id': 1,
#         'type_of_process': 'TEST_RESCISÃO SEM JUSTA CAUSA',
#         'department': 'Departamento de Pessoal',
#     }
