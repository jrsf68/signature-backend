\c postgres
CREATE SCHEMA IF NOT EXISTS signature_backend;
CREATE TABLE IF NOT EXISTS signature_backend.types_of_process (
    id SERIAL PRIMARY KEY,
    type_of_process TEXT UNIQUE,
    department TEXT
);

CREATE TABLE IF NOT EXISTS signature_backend.type_of_process_pdf_files (
    id SERIAL PRIMARY KEY,
    type_of_process_id INTEGER REFERENCES signature_backend.types_of_process(id),
    file_description TEXT,
    id_tags_in_text TEXT,
    required_file BOOLEAN
);

-- psql -d postgres -f signature_backend/create.sql
