from fastapi import APIRouter

from signature_backend.application.type_of_process import (
    create_type_of_process,
    get_type_of_process,
)
from signature_backend.domain.entities import TypesOfProcess

router = APIRouter()


@router.get('/')
async def read_root() -> dict:
    """Application root route.

    Returns:
        dict: dictionary to be serialized and sent in json format.
    """
    return {'status': 'Server it is running!'}


@router.post('/type_of_process/')
async def post_type_of_process(type_of_process: dict):
    """Create a type of process.

    Args:
        type_of_process (TypesOfProcess): type of process data.
    """

    await create_type_of_process(type_of_process)
    return {'message': 'Item created successfully'}


@router.get('/type_of_process/{id}')
async def get_top(id: int) -> dict:
    """Read a type of process.

    Args:
        id (int): id of process.

    Returns:
        dict: dict of type of processs data read.
    """

    result_set = await get_type_of_process(id)

    return result_set
